package com.joshuait.springctx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class MessagePrinter {
	
	//3.通过属性直接注入
	@Autowired
    private MessageService service;
	
    //1. 通过构造方法注入
   /* @Autowired
    public MessagePrinter(MessageService service) {
        this.service = service;
    }*/
    

   /* public MessageService getService() {
		return service;
	}

    //2. 通过setter注入
    @Autowired
	public void setService(MessageService service) {
		this.service = service;
	}*/



	public void printMessage() {
        System.out.println(this.service.getMessage());
    }
}
