<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
	<!-- 新 Bootstrap 核心 CSS 文件 -->
	<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css">
	
	<!-- 可选的Bootstrap主题文件（一般不用引入） -->
	<link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	
	<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
	<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
	
	<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
	<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container-fluid">
		<h1>用户管理</h1>
		<c:if test="${!empty successMsg}">
			<p class="alert alert-success" role="alert">${successMsg}</p>
		</c:if>
		<c:if test="${!empty errorMsg}">
			<p class="alert alert-danger" role="alert">${successMsg}</p>
		</c:if>
		<table class="table table-hover table-condensed table-bordered">
			<tr>
				<th>id</th>
				<th>userLogin</th>
				<th>userNicename</th>
				<th>userEmail</th>
				<th>userRegistered</th>
				<th>displayName</th>
				<th>Operation</th>
			</tr>
			<c:forEach items="${userList}" var="user">
				<tr>
					<td>${user.id}</td>
					<td>${user.userLogin}</td>
					<td>${user.userNicename}</td>
					<td>${user.userEmail}</td>
					<td>${user.userRegistered}</td>
					<td>${user.displayName}</td>
					<td><a href="${ctx}/user/delete/${user.id}">删除</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>
