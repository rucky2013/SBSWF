package com.joshuait.sbswf.dao;

import java.util.List;

import com.joshuait.sbswf.entity.User;

public interface UserMapper {

	public User getById(int id);
	
	public List<User> search(User user);
	
	public void insert(User user);

	public void update(User user);
	
	public void delete(int id);

}
