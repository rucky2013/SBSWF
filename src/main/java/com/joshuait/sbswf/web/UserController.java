package com.joshuait.sbswf.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.joshuait.sbswf.entity.User;
import com.joshuait.sbswf.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping
	public String list(Model model) {
		logger.info("start");
		List<User> userList = userService.search(null);
		model.addAttribute("userList", userList);
		model.addAttribute("successMsg", "查询成功");
		logger.info("end");
		return "user/user-list";
	}
	
	@RequestMapping(value="delete/{id}",method = RequestMethod.GET)
	public String delete(@PathVariable Integer id,Model model) {
		userService.delete(id);
		logger.info(id.toString());
		model.addAttribute("successMsg", "删除成功");
		return "forward:/user";
	}
}
