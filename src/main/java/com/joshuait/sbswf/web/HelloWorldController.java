package com.joshuait.sbswf.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/hello")
public class HelloWorldController {

	@RequestMapping
    public String helloWorld(Model model) {
        model.addAttribute("message", "Hello World!2222");
        return "helloWorld";
    }
	
	@RequestMapping(value="hello2")
    public String helloWorld2(Model model) {
        model.addAttribute("message", "Hello World!555555555");
        return "helloWorld";
    }
}